1. Create docker compose configuration that sets up mongo cluster and mongo-express UI
2. Add endpoints for updating the resource (name, description, capacity) with validation
3. Add unit test for ResourceAPI using WebFluxTest
4. Add more integration tests to ResourceServiceApplicationTests
