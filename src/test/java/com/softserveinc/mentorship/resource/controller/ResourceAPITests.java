package com.softserveinc.mentorship.resource.controller;

import com.softserveinc.mentorship.resource.model.CreateResourceRequest;
import com.softserveinc.mentorship.resource.model.PartiesRequest;
import com.softserveinc.mentorship.resource.model.Resource;
import com.softserveinc.mentorship.resource.model.UpdateResourceRequest;
import com.softserveinc.mentorship.resource.service.ResourceOperations;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@WebFluxTest(ResourceAPI.class)
class ResourceAPITests {

    @MockBean
    private ResourceOperations resources;

    @Autowired
    private WebTestClient webClient;

    @Test
    void create_validRequest_validationPassed() {
        // arrange
        CreateResourceRequest createResourceRequest = createResourceRequest("Test name", null, 10);
        when(resources.create(createResourceRequest)).thenReturn(createResource(createResourceRequest));

        // act & assert
        webClient.post()
                .uri("/api/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(createResourceRequest)
                .exchange()
                .expectStatus().isCreated();

        verify(resources, only()).create(createResourceRequest);
    }

    @Test
    void create_nameIsBlank_validationFailed() {
        // arrange
        CreateResourceRequest createResourceRequest = createResourceRequest("", "Test description", 10);

        // act & assert
        webClient.post()
                .uri("/api/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(createResourceRequest)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.errors[0].code").isEqualTo("NotBlank");

        verifyNoInteractions(resources);
    }

    @Test
    void create_capacityNotPositive_validationFailed() {
        // arrange
        CreateResourceRequest createResourceRequest = createResourceRequest("Test name", "Test description", 0);

        // act & assert
        webClient.post()
                .uri("/api/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(createResourceRequest)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.errors[0].code").isEqualTo("Positive");

        verifyNoInteractions(resources);
    }

    @Test
    void update_validRequest_validationPassed() {
        // arrange
        String resourceId = "42";
        UpdateResourceRequest updateResourceRequest = createUpdateRequest("Test name", "Test description", 10);
        when(resources.update(resourceId, false, updateResourceRequest)).thenReturn(createResource(updateResourceRequest));

        // act & assert
        webClient.patch()
                .uri("/api/resources/{id}", resourceId)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(updateResourceRequest)
                .exchange()
                .expectStatus().isOk();

        verify(resources, only()).update(resourceId, false, updateResourceRequest);
    }

    @Test
    void update_descriptionIsBlankAndCapacityIsNegative_validationPassed() {
        // arrange
        String resourceId = "42";
        UpdateResourceRequest updateResourceRequest = createUpdateRequest("Test name blank", "", -1);
        when(resources.update(resourceId, false, updateResourceRequest)).thenReturn(createResource(updateResourceRequest));

        // act & assert
        webClient.patch()
                .uri("/api/resources/{id}", resourceId)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(updateResourceRequest)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.errors[*].code").value(containsInAnyOrder("Positive", "NotBlank", "Null"));

        verifyNoInteractions(resources);
    }

    @Test
    void acquireOrRelease_acquireResource_returnResource() {
        // arrange
        String resourceId = "42";
        int parties = 4;
        PartiesRequest partiesRequest = createPartiesRequest(PartiesRequest.Mode.ACQUIRE, parties);
        when(resources.acquire(resourceId, parties)).thenReturn(createResource());

        // act & assert
        webClient.patch()
                .uri("/api/resources/{id}/parties", resourceId)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(partiesRequest)
                .exchange()
                .expectStatus().isOk();

        verify(resources, only()).acquire(resourceId, partiesRequest.getParties());
    }

    @Test
    void acquireOrRelease_releaseResource_returnResource() {
        // arrange
        String resourceId = "42";
        int parties = 4;
        PartiesRequest partiesRequest = createPartiesRequest(PartiesRequest.Mode.RELEASE, parties);
        when(resources.release(resourceId, parties)).thenReturn(createResource());

        // act & assert
        webClient.patch()
                .uri("/api/resources/{id}/parties", resourceId)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(partiesRequest)
                .exchange()
                .expectStatus().isOk();

        verify(resources, only()).release(resourceId, partiesRequest.getParties());
    }

    @Test
    void acquireOrRelease_partiesNotPositive_validationFailed() {
        // arrange
        String resourceId = "42";
        int parties = -4;
        PartiesRequest partiesRequest = createPartiesRequest(PartiesRequest.Mode.ACQUIRE, parties);

        // act & assert
        webClient.patch()
                .uri("/api/resources/{id}/parties", resourceId)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(partiesRequest)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.errors[*].code").isEqualTo("Positive");

        verifyNoInteractions(resources);
    }

    @Test
    void acquireOrRelease_notExistingId_returnNotFoundStatus() {
        // arrange
        String resourceId = "42";
        int parties = 4;
        PartiesRequest partiesRequest = createPartiesRequest(PartiesRequest.Mode.ACQUIRE, parties);
        when(resources.acquire(resourceId, parties)).thenReturn(Mono.empty());

        // act & assert
        webClient.patch()
                .uri("/api/resources/{id}/parties", resourceId)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(partiesRequest)
                .exchange()
                .expectStatus().isNotFound()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Resource { 42 } was not found");

        verify(resources, only()).acquire(resourceId, partiesRequest.getParties());
    }

    @Test
    void findById_existingId_returnResource() {
        // arrange
        String resourceId = "42";
        when(resources.findById(resourceId)).thenReturn(createResource());

        // act & assert
        webClient.get()
                .uri("/api/resources/{id}", resourceId)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.name").isEqualTo("Test name")
                .jsonPath("$.capacity").isEqualTo(10);

        verify(resources, only()).findById(resourceId);
    }

    @Test
    void findById_notExistingId_returnNotFoundStatus() {
        // arrange
        String resourceId = "42";
        when(resources.findById(resourceId)).thenReturn(Mono.empty());

        // act & assert
        webClient.get()
                .uri("/api/resources/{id}", resourceId)
                .exchange()
                .expectStatus().isNotFound()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Resource { 42 } was not found");

        verify(resources, only()).findById(resourceId);
    }

    @Test
    void deleteById_returnNoContentStatus() {
        // arrange
        String resourceId = "42";
        Mono<Void> voidReturn  = Mono.empty();
        when(resources.delete(resourceId)).thenReturn(voidReturn);

        // act & arrange
        webClient.delete()
                .uri("/api/resources/{id}", 42)
                .exchange()
                .expectStatus().isNoContent();

        verify(resources, only()).delete(resourceId);
    }

    private CreateResourceRequest createResourceRequest(String name, String description, int capacity) {
        return new CreateResourceRequest(name, description, capacity);
    }

    private UpdateResourceRequest createUpdateRequest(String name, String description, int capacity) {
        return new UpdateResourceRequest(name, description, capacity);
    }

    private PartiesRequest createPartiesRequest(PartiesRequest.Mode mode, int parties) {
        return new PartiesRequest(mode, parties);
    }

    private Mono<Resource> createResource(CreateResourceRequest request) {
        return Mono.just(Resource.create(
                request.getName(),
                request.getDescription(),
                request.getCapacity()
        ));
    }

    private Mono<Resource> createResource(UpdateResourceRequest request) {
        return Mono.just(Resource.create(
                request.getName(),
                request.getDescription(),
                request.getCapacity()
        ));
    }

    private Mono<Resource> createResource() {
        return Mono.just(Resource.create("Test name", null, 10));
    }

}
