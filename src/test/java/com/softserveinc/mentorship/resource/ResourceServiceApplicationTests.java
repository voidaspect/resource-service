package com.softserveinc.mentorship.resource;

import com.softserveinc.mentorship.resource.model.CreateResourceRequest;
import com.softserveinc.mentorship.resource.model.PartiesRequest;
import com.softserveinc.mentorship.resource.model.Resource;
import com.softserveinc.mentorship.resource.model.UpdateResourceRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class ResourceServiceApplicationTests {

    @Autowired
    private WebTestClient client;

    @Test
    void healthEndpoint_ShouldReturn_Status_UP() {
        client.get().uri("/actuator/health")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("UP");
    }

    @Test
    void shouldCreateResource() {
        var createResourceRequest = new CreateResourceRequest("test-1", "test-1-desc", 3);

        client.post().uri("/api/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(createResourceRequest)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.name").isEqualTo(createResourceRequest.getName())
                .jsonPath("$.description").isEqualTo(createResourceRequest.getDescription())
                .jsonPath("$.capacity").isEqualTo(createResourceRequest.getCapacity())
                .jsonPath("$.parties").isEqualTo(0);
    }

    @Test
    void shouldUpdateResource() {
        var createResourceRequest = new CreateResourceRequest("test-1", "test-1-desc", 10);

        Resource resource = createResource(createResourceRequest);

        String id = resource.getId();
        var updateResourceRequest = new UpdateResourceRequest("test-1-updated", "test-1-desc-updated", 5);
        client.patch().uri("/api/resources/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(updateResourceRequest)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo(id)
                .jsonPath("$.name").isEqualTo(updateResourceRequest.getName())
                .jsonPath("$.description").isEqualTo(updateResourceRequest.getDescription())
                .jsonPath("$.capacity").isEqualTo(updateResourceRequest.getCapacity());
    }

    @Test
    void shouldUpdateResource_dontDropOptionalFields() {
        var createResourceRequest = new CreateResourceRequest("test-2", "test-2-desc", 10);

        Resource resource = createResource(createResourceRequest);

        String id = resource.getId();
        var updateResourceRequest = new UpdateResourceRequest("test-2-updated", null, 5);
        client.patch().uri("/api/resources/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(updateResourceRequest)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo(id)
                .jsonPath("$.name").isEqualTo(updateResourceRequest.getName())
                .jsonPath("$.description").isEqualTo(resource.getDescription())
                .jsonPath("$.capacity").isEqualTo(updateResourceRequest.getCapacity());
    }

    @Test
    void shouldUpdateResource_dropOptionalFields() {
        var createResourceRequest = new CreateResourceRequest("test-3", "test-3-desc", 10);

        Resource resource = createResource(createResourceRequest);

        String id = resource.getId();
        var updateResourceRequest = new UpdateResourceRequest("test-3-updated", null, 5);
        client.patch().uri("/api/resources/{id}?drop-optional-fields=true", id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(updateResourceRequest)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo(id)
                .jsonPath("$.name").isEqualTo(updateResourceRequest.getName())
                .jsonPath("$.description").isEmpty()
                .jsonPath("$.capacity").isEqualTo(updateResourceRequest.getCapacity());
    }

    @Test
    void shouldAcquireResource() {
        var createResourceRequest = new CreateResourceRequest("test-1", null, 5);

        Resource resource = createResource(createResourceRequest);

        String id = resource.getId();
        var acquireResource = new PartiesRequest(PartiesRequest.Mode.ACQUIRE, 3);
        client.patch().uri("/api/resources/{id}/parties", id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(acquireResource)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo(id)
                .jsonPath("$.capacity").isEqualTo(5)
                .jsonPath("$.parties").isEqualTo(3);

        client.patch().uri("/api/resources/{id}/parties", id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(acquireResource)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Resource { " + id + " } is not available");
    }

    @Test
    void shouldReleaseResource() {
        var createResourceRequest = new CreateResourceRequest("test-1", null, 5);

        Resource resource = createResource(createResourceRequest);

        String id = resource.getId();
        var acquireResource = new PartiesRequest(PartiesRequest.Mode.ACQUIRE, 3);
        client.patch().uri("/api/resources/{id}/parties", id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(acquireResource)
                .exchange()
                .expectStatus().isOk();

        var releaseResource = new PartiesRequest(PartiesRequest.Mode.RELEASE, 4);
        client.patch().uri("/api/resources/{id}/parties", id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(releaseResource)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo(id)
                .jsonPath("$.capacity").isEqualTo(5)
                .jsonPath("$.parties").isEqualTo(0);
    }

    @Test
    void shouldRetrieveResourceById() {
        var createResourceRequest = new CreateResourceRequest("test-2", "test-2-desc", 3);

        Resource resource = createResource(createResourceRequest);

        String id = resource.getId();

        client.get().uri("/api/resources/{id}", id)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Resource.class)
                .isEqualTo(resource);
    }

    @Test
    void shouldDeleteResourceById() {
        var createResourceRequest = new CreateResourceRequest("test-3", "test-3-desc", 3);

        Resource resource = createResource(createResourceRequest);

        String id = resource.getId();

        client.delete().uri("/api/resources/{id}", id)
                .exchange()
                .expectStatus().isNoContent()
                .expectBody().isEmpty();

        client.delete().uri("/api/resources/{id}", id)
                .exchange()
                .expectStatus().isNoContent()
                .expectBody().isEmpty();

        client.get().uri("/api/resources/{id}", id)
                .exchange()
                .expectStatus().isNotFound();
    }

    private Resource createResource(CreateResourceRequest createResourceRequest) {
        return client.post().uri("/api/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(createResourceRequest)
                .exchange()
                .expectBody(Resource.class)
                .returnResult()
                .getResponseBody();
    }
}
