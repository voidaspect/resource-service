package com.softserveinc.mentorship.resource.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ResourceUnavailableException extends ResponseStatusException {

    public ResourceUnavailableException(String id) {
        super(HttpStatus.CONFLICT, "Resource { " + id  + " } is not available");
    }

}
