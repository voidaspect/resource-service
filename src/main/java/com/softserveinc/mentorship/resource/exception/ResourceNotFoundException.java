package com.softserveinc.mentorship.resource.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ResourceNotFoundException extends ResponseStatusException {

    public ResourceNotFoundException(String id) {
        super(HttpStatus.NOT_FOUND, "Resource { " + id + " } was not found");
    }
}
