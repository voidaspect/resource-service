package com.softserveinc.mentorship.resource.repository;

import com.softserveinc.mentorship.resource.model.Resource;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ResourceRepository extends ReactiveMongoRepository<Resource, String> {
}
