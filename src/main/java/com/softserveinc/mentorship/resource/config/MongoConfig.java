package com.softserveinc.mentorship.resource.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@EnableReactiveMongoRepositories(basePackages = "com.softserveinc.mentorship.resource.repository")
@Configuration(proxyBeanMethods = false)
public class MongoConfig {
}
