package com.softserveinc.mentorship.resource.service;

import com.softserveinc.mentorship.resource.model.CreateResourceRequest;
import com.softserveinc.mentorship.resource.model.Resource;
import com.softserveinc.mentorship.resource.model.UpdateResourceRequest;
import reactor.core.publisher.Mono;

public interface ResourceOperations {

    Mono<Resource> findById(String id);

    Mono<Resource> acquire(String id, int parties);

    Mono<Resource> release(String id, int parties);

    Mono<Resource> create(CreateResourceRequest request);

    Mono<Resource> update(String id, boolean dropOptionalFields, UpdateResourceRequest request);

    Mono<Void> delete(String id);

}
