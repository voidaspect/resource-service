package com.softserveinc.mentorship.resource.service;

import com.softserveinc.mentorship.resource.model.CreateResourceRequest;
import com.softserveinc.mentorship.resource.model.Resource;
import com.softserveinc.mentorship.resource.model.UpdateResourceRequest;
import com.softserveinc.mentorship.resource.repository.ResourceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ResourceService implements ResourceOperations {

    private final ResourceRepository resourceRepository;

    public ResourceService(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    @Override
    public Mono<Resource> findById(String id) {
        return resourceRepository.findById(id);
    }

    @Override
    @Transactional
    public Mono<Resource> acquire(String id, int parties) {
        return resourceRepository.findById(id)
                .flatMap(resource -> resourceRepository.save(resource.acquire(parties)));
    }

    @Override
    @Transactional
    public Mono<Resource> release(String id, int parties) {
        return resourceRepository.findById(id)
                .flatMap(resource -> resourceRepository.save(resource.release(parties)));
    }

    @Override
    @Transactional
    public Mono<Resource> create(CreateResourceRequest request) {
        var resource = Resource.create(
                request.getName(),
                request.getDescription(),
                request.getCapacity()
        );
        return resourceRepository.save(resource);
    }

    @Override
    @Transactional
    public Mono<Resource> update(String id, boolean dropOptionalFields, UpdateResourceRequest request) {
        return resourceRepository.findById(id).flatMap(resource -> resourceRepository.save(resource.update(
                Objects.requireNonNullElseGet(request.getName(), resource::getName),
                Optional.ofNullable(request.getDescription())
                        .orElseGet(() -> dropOptionalFields ? null : resource.getDescription()),
                Objects.requireNonNullElseGet(request.getCapacity(), resource::getCapacity)
        )));
    }

    @Override
    @Transactional
    public Mono<Void> delete(String id) {
        return resourceRepository.deleteById(id);
    }
}
