package com.softserveinc.mentorship.resource.controller;

import com.softserveinc.mentorship.resource.model.CreateResourceRequest;
import com.softserveinc.mentorship.resource.model.PartiesRequest;
import com.softserveinc.mentorship.resource.model.Resource;
import com.softserveinc.mentorship.resource.model.UpdateResourceRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RequestMapping("/api/resources")
@Validated
public interface ResourceAPI {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Mono<ResponseEntity<Resource>> create(@RequestBody @Valid CreateResourceRequest request, UriComponentsBuilder ucb);

    @PatchMapping("/{id}")
    Mono<Resource> update(@NotBlank @PathVariable String id,
                          @RequestParam(value = "drop-optional-fields", defaultValue = "false") boolean dropOptionalFields,
                          @RequestBody @Valid UpdateResourceRequest request);

    @PatchMapping("/{id}/parties")
    Mono<Resource> acquireOrRelease(@NotBlank @PathVariable String id, @RequestBody @Valid PartiesRequest request);

    @GetMapping("/{id}")
    Mono<Resource> findById(@NotBlank @PathVariable String id);

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    Mono<Void> deleteById(@NotBlank @PathVariable String id);

}
