package com.softserveinc.mentorship.resource.controller;

import com.softserveinc.mentorship.resource.exception.ResourceNotFoundException;
import com.softserveinc.mentorship.resource.model.CreateResourceRequest;
import com.softserveinc.mentorship.resource.model.PartiesRequest;
import com.softserveinc.mentorship.resource.model.Resource;
import com.softserveinc.mentorship.resource.model.UpdateResourceRequest;
import com.softserveinc.mentorship.resource.service.ResourceOperations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

@RestController
public class ResourceController implements ResourceAPI {

    private final ResourceOperations resources;

    public ResourceController(ResourceOperations resources) {
        this.resources = resources;
    }

    @Override
    public Mono<ResponseEntity<Resource>> create(CreateResourceRequest request, UriComponentsBuilder ucb) {
        return resources.create(request).map(resource -> ResponseEntity
                .created(ucb.path("/api/resources/" + resource.getId()).build().toUri())
                .body(resource));
    }

    @Override
    public Mono<Resource> update(String id, boolean dropOptionalFields, UpdateResourceRequest request) {
        return resources.update(id, dropOptionalFields, request);
    }

    @Override
    public Mono<Resource> acquireOrRelease(String id, PartiesRequest request) {
        var resource = switch (request.getMode()) {
            case ACQUIRE -> resources.acquire(id, request.getParties());
            case RELEASE -> resources.release(id, request.getParties());
        };
        return resource.switchIfEmpty(notFound(id));
    }

    @Override
    public Mono<Resource> findById(String id) {
        return resources.findById(id).switchIfEmpty(notFound(id));
    }

    @Override
    public Mono<Void> deleteById(String id) {
        return resources.delete(id);
    }

    private static Mono<Resource> notFound(String id) {
        return Mono.error(() -> new ResourceNotFoundException(id));
    }
}
