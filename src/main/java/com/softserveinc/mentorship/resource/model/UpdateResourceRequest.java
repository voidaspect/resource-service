package com.softserveinc.mentorship.resource.model;

import com.softserveinc.mentorship.resource.validation.constraint.NullOrNotBlank;
import lombok.Value;

import javax.validation.constraints.Positive;

@Value
public class UpdateResourceRequest {

    @NullOrNotBlank
    String name;

    @NullOrNotBlank
    String description;

    @Positive
    Integer capacity;

}
