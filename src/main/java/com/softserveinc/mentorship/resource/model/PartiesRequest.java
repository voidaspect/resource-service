package com.softserveinc.mentorship.resource.model;

import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Value
public class PartiesRequest {

    @NotNull
    Mode mode;

    @Positive
    int parties;

    public enum Mode {
        ACQUIRE, RELEASE
    }

}
