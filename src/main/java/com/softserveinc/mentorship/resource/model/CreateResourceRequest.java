package com.softserveinc.mentorship.resource.model;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Value
public class CreateResourceRequest {

    @NotBlank
    String name;

    String description;

    @Positive
    int capacity;

}
