package com.softserveinc.mentorship.resource.model;

import com.softserveinc.mentorship.resource.exception.ResourceUnavailableException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Resource {

    @Id
    private final String id;

    private final String name;

    private final String description;

    private final int capacity;

    private final int parties;

    public static Resource create(String name, String description, int capacity) {
        return new Resource(null, name, description, capacity, 0);
    }

    public Resource update(String name, String description, int capacity) {
        return new Resource(id, name, description, capacity, parties);
    }

    public Resource acquire(int parties) {
        if (parties <= 0) {
            throw new IllegalArgumentException("Number of parties acquiring the resource should be positive");
        }
        int totalParties = this.parties + parties;
        if (totalParties > capacity) {
            throw new ResourceUnavailableException(id);
        }
        return withParties(totalParties);
    }

    public Resource release(int parties) {
        if (parties <= 0) {
            throw new IllegalArgumentException("Number of parties releasing the resource should be positive");
        }
        int totalParties = Math.max(this.parties - parties, 0);
        return withParties(totalParties);
    }

    private Resource withParties(int parties) {
        return new Resource(
                id,
                name,
                description,
                capacity,
                parties
        );
    }

}
